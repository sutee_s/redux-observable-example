const getCallAPIAction = action => ({
  REQUEST: `${action}_REQUEST`,
  SUCCESS: `${action}_SUCCESS`,
  FAILURE: `${action}_FAILURE`,
})

export const makeActionCreator = (type, ...argNames) => (...args) => {
  let action = { type }
  argNames.forEach((arg, index) => {
    action[arg] = args[index]
  })
  return action
}

export const PING = 'PING'
export const PONG = 'PONG'

export const ACTIVITY = getCallAPIAction('ACTIVITY')
export const LOGOUT = 'LOGOUT'

export const GET_MOVIE_LIST = getCallAPIAction('GET_MOVIE_LIST')
export const SEARCH_MOVIE_LIST = getCallAPIAction('SEARCH_MOVIE_LIST')
