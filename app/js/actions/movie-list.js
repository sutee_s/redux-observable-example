import {
  makeActionCreator,
  GET_MOVIE_LIST,
  SEARCH_MOVIE_LIST,
} from './action-types'

export const getMovieListRequest = makeActionCreator(GET_MOVIE_LIST.REQUEST, 'params')
export const getMovieListSuccess = makeActionCreator(GET_MOVIE_LIST.SUCCESS, 'payload')
export const getMovieListFailure = makeActionCreator(GET_MOVIE_LIST.FAILURE, 'error')

export const searchMovieListRequest = makeActionCreator(SEARCH_MOVIE_LIST.REQUEST, 'params')
export const searchMovieListSuccess = makeActionCreator(SEARCH_MOVIE_LIST.SUCCESS, 'payload')
export const searchMovieListFailure = makeActionCreator(SEARCH_MOVIE_LIST.FAILURE, 'error')
