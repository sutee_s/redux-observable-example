import { ACTIVITY, makeActionCreator } from './action-types'

export const getActivityRequest = makeActionCreator(ACTIVITY.REQUEST)
export const getActivitySuccess = makeActionCreator(ACTIVITY.SUCCESS)
