import { LOGOUT, makeActionCreator } from './action-types'

export const doLogout = makeActionCreator(LOGOUT)
