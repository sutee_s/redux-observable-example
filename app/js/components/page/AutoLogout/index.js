import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getActivityRequest } from '../../../actions/activity'

import './index.scss'

class Home extends Component {
  componentDidMount() {
    this.doActivity()
  }

  componentDidUpdate(prevProps) {
    const { isLogout } = this.props
    if (isLogout !== prevProps.isLogout && isLogout) {
      alert('auto logout!!!')
    }
  }

  doActivity = () => {
    this.props.doActivity()
  }

  render() {
    return (
      <div className="home-container">
        <img src='/static/images/logo.png' />
        <div className="red-text">Hello from scale360</div>
        <div>
          has activity ? {String(this.props.isActive)}
        </div>
        <button onClick={this.doActivity}>click to send activity</button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isActive: state.activity,
  isLogout: state.logout
})

const mapDispatchToProps = dispatch => ({
  doActivity: () => { dispatch(getActivityRequest()) },
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
