import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './index.scss'

class Home extends Component {
  render() {
    return (
      <div className="home-container">
        <img src='/static/images/logo.png' />
        <div className="red-text">Content</div>
        <div><Link to="/test">basic</Link></div>
        <div><Link to="/movies">call API</Link></div>
        <div><Link to="/search-movie">search and debounce time</Link></div>
        <div><Link to="/auto-logout">auto logout</Link></div>
      </div>
    )
  }
}

export default Home
