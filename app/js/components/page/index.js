import Home from './Home'
import Test from './Test'
import AutoLogout from './AutoLogout'
import MovieList from './MovieList'
import SearchMovie from './SearchMovie'

export {
  Home,
  Test,
  AutoLogout,
  MovieList,
  SearchMovie,
}
