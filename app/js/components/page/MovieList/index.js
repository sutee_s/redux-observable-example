import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getMovieListRequest } from '../../../actions/movie-list'
import './index.scss'

class MovieList extends Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    requestMovies: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const params = {
      s: 'star',
    }
    this.props.requestMovies(params)
  }

  render() {
    return (
      <div className="movie-list">
        <h1>Star* Movie List</h1>
        {
          this.props.list.map(movie => (
            <p key={movie.imdbID}>{movie.Title}</p>
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  list: state.movieList.data
})

const mapDisPatchToProps = dispatch => ({
  requestMovies: (params) => { dispatch(getMovieListRequest(params)) }
})

export default connect(mapStateToProps, mapDisPatchToProps)(MovieList)
