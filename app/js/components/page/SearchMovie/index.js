import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { searchMovieListRequest, searchMovieListSuccess } from '../../../actions/movie-list'
import './index.scss'

class SearchMovie extends Component {
  static propTypes = {
    movieList: PropTypes.object.isRequired,
    searchMovie: PropTypes.func.isRequired,
  }

  onChangeHandler = (e) => {
    const { searchMovie, clearMovie } = this.props
    const { value } = e.target
    if (value.length > 1) {
      const params = {
        s: value,
      }
      searchMovie(params)
    } else {
      clearMovie()
    }
  }

  renderMovies() {
    const { isError, isLoading, data } = this.props.movieList
    if (isError) {
      return null
    } else if (isLoading) {
      return <div>Loading...</div>
    } else if (this.searchInput && this.searchInput.value.length > 1) {
      return (
        <div>
          {
            data.map(movie => (
              <p key={movie.imdbID}>{movie.Title}</p>
            ))
          }
        </div>
      )
    }
    return null
  }

  render() {
    return (
      <div className="search-movie-wrapper">
        <div className="input-part">
          <input
            ref={(input) => { this.searchInput = input }}
            type="text"
            className="input-search"
            placeholder="movie name..."
            onChange={this.onChangeHandler}
          />
        </div>
        <div className="desc-part">
          <h4><i>Please action in logs</i></h4>
        </div>
        <div className="movie-list-part">
          {this.renderMovies()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  movieList: state.movieList,
})

const mapDisPatchToProps = dispatch => ({
  searchMovie: (params) => { dispatch(searchMovieListRequest(params)) },
  clearMovie: () => { dispatch(searchMovieListSuccess([])) },
})

export default connect(mapStateToProps, mapDisPatchToProps)(SearchMovie)
