import React, { Component } from 'react'
import { Route, BrowserRouter as Router, Switch, BrowserRouter } from 'react-router-dom'

import {
  Home,
  Test,
  AutoLogout,
  MovieList,
  SearchMovie,
} from './components/page'

class Root extends Component {
  render() {
    return (
      <div>
        <Router>
          <BrowserRouter>
            <Switch>
              <Route path='/' exact component={ Home } />
              <Route path='/test' exact component={ Test } />
              <Route path='/auto-logout' exact component={ AutoLogout } />
              <Route path='/movies' exact component={ MovieList } />
              <Route path='/search-movie' exact component={ SearchMovie } />
              <Route path='*' render={ () => <h1>Page Not Found</h1> } />
            </Switch>
          </BrowserRouter>
        </Router>
      </div>
    )
  }
}

export default Root
