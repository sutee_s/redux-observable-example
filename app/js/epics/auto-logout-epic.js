import { endsWith } from 'lodash'

import { doLogout } from '../actions/logout'

export default action$ =>
  action$.filter(action => endsWith(action.type, '_REQUEST'))
    .debounceTime(5 * 1000)
    .mapTo(doLogout())
