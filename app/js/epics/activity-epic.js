import { ACTIVITY } from '../actions/action-types'
import { getActivitySuccess } from '../actions/activity'

const getActivityRequestEpic = action$ =>
  action$.ofType(ACTIVITY.REQUEST)
    .delay(300)
    .mapTo(getActivitySuccess())

export default getActivityRequestEpic
