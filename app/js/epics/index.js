import { combineEpics } from 'redux-observable'

import errorEpic from './error-epic'
import pingPongEpic from './ping-pong-epic'
import activityEpic from './activity-epic'
import autoLogoutEpic from './auto-logout-epic'
import movieListEpic from './movie-list-epic'

export default combineEpics(
  errorEpic,
  pingPongEpic,
  activityEpic,
  autoLogoutEpic,
  movieListEpic,
)
