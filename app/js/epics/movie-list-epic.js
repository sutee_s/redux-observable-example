import { Observable } from 'rxjs'
import { combineEpics } from 'redux-observable'

import { GET_MOVIE_LIST, SEARCH_MOVIE_LIST } from '../actions/action-types'
import * as actions from '../actions/movie-list'

const getMovieListEpic = (action$, store, { callStaticAPI }) =>
  action$.ofType(GET_MOVIE_LIST.REQUEST)
    .mergeMap(action =>
      callStaticAPI({
        url: 'http://www.omdbapi.com',
        method: 'GET',
        body: {
          apikey: 'ae3ea9f6',
          ...action.params,
        },
        headers: {},
      })
        .map(result => actions.getMovieListSuccess(result.response.Search))
        .catch(error => Observable.of(actions.getMovieListFailure(error)))
    )

const searchMovieListEpic = (action$, store, { callStaticAPI }) =>
  action$.ofType(SEARCH_MOVIE_LIST.REQUEST)
    .debounceTime(500)
    .switchMap(action =>
      callStaticAPI({
        url: 'http://www.omdbapi.com',
        method: 'GET',
        body: {
          apikey: 'ae3ea9f6',
          ...action.params,
        },
        headers: {},
      })
        .map(result => actions.searchMovieListSuccess(result.response.Search))
        .catch(error => Observable.of(actions.searchMovieListFailure(error)))
    )

export default combineEpics(
  getMovieListEpic,
  searchMovieListEpic,
)
