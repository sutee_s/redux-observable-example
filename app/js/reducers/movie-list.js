import { GET_MOVIE_LIST, SEARCH_MOVIE_LIST } from '../actions/action-types'

const initialState = {
  isLoading: false,
  isError: false,
  data: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_MOVIE_LIST.REQUEST:
    case GET_MOVIE_LIST.REQUEST:
      return {
        isLoading: true,
        isError: false,
        data: []
      }
    case SEARCH_MOVIE_LIST.SUCCESS:
    case GET_MOVIE_LIST.SUCCESS:
      return {
        isLoading: false,
        isError: false,
        data: action.payload || []
      }
    case SEARCH_MOVIE_LIST.FAILURE:
    case GET_MOVIE_LIST.FAILURE:
      return {
        isLoading: false,
        isError: true,
        data: []
      }
    default:
      return state
  }
}
