import { combineReducers } from 'redux'

import error from './error'
import isPing from './ping-pong'
import activity from './activity'
import logout from './logout'
import movieList from './movie-list'

const rootReducer = combineReducers({
  error,
  isPing,
  activity,
  logout,
  movieList,
})

export default rootReducer
