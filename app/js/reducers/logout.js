import { LOGOUT } from '../actions/action-types'

export default (state = false, action) => {
  switch (action.type) {
    case LOGOUT:
      return true
    default:
      return state
  }
}
