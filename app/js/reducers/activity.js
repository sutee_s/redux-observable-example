import { ACTIVITY } from '../actions/action-types'

export default (state = false, action) => {
  switch (action.type) {
    case ACTIVITY.REQUEST:
      return true
    case ACTIVITY.SUCCESS:
      return false
    default:
      return state
  }
}
